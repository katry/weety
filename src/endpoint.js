/*jshint esversion: 8 */

window.endpoint = new class {
	#regexEndpoints = {};
	#endpoints = {};

	constructor() {
		const _fetch = fetch;
		const _XMLHttpRequestOpen = XMLHttpRequest.prototype.open;
		window.fetch = (req, opt={}, skip=false) => {
			const url = req instanceof Request ? req.url : req;
			const endpoints = this.#getEndpoints(url);
			if(!skip && endpoints.length) {
				return _fetch(req, opt).then(res => {
					const clone = res.clone();
					if(res.status > 399)
						for(let i in endpoints)
							endpoints[i].error(url, res.status, res.statusText);
					else res.text().then(data => {
						try { data = JSON.parse(data); }
						catch(e) {}
						for(let i in endpoints)
							endpoints[i].load(req.method || opt.method || "GET", data);
					});
					return clone;
				});
			}
			else return _fetch(req, opt);
		};

		XMLHttpRequest.prototype.open = ((context) => {
			return function(method, url, async, user, password, skip) {
				const r = _XMLHttpRequestOpen.apply(this, [].slice.call(arguments));
				const endpoints = context.#getEndpoints(url);
				if(!skip && endpoints.length) {
					this.addEventListener("readystatechange", (evt) => {
						if(this.readyState === XMLHttpRequest.DONE) {
							if(this.status > 399) {
								for(let i in endpoints)
									endpoints[i].error(url, this.status, this.statusText);
								return;
							}
							let data = evt.responseText;
							try {data = JSON.parse(data);}
							catch(e) {}
							for(let i in endpoints)
								endpoints[i].load(method, data);
						}
					});
				}
				return r;
			};
		})(this);
	}

	get(pattern) {
		const input = this.#validate(pattern);
		return input.length ? input[2][input[1]] = input[2][input[1]] || new (class {
			#pattern;
			#listeners = {load: {}, error: []};

			constructor(pattern) {
				this.#pattern = pattern;
			}

			addEventListener(type, callback, method="GET") {
				if(typeof callback !== "function")
					throw new TypeError("Invalid callback");
				if(!this.#listeners[type])
					throw new TypeError(`Invalid listener ${type}`);
				if(type === "load") {
					if(!this.#listeners[type][method])
						this.#listeners[type][method] = [];
					this.#listeners[type][method].push(callback);
				}
				else this.#listeners[type].push(callback);
			}

			removeEventListener(type, callback, method="GET") {
				if(!this.#listeners[type])
					throw new TypeError( `Invalid listener ${type}`);
				if(type === "load")
					this.#listeners[type][method] = (this.#listeners[type][method] || []).forEach(e => e!==callback);
				else this.#listeners[type] = this.#listeners[type].filter(e => e!==callback);
			}

			load(method, data) {
				(this.#listeners.load[method] || []).forEach(i => i(data));
			}

			error(url, status, statusText="") {
				this.#listeners.error.forEach(i => i(url, status, statusText));
			}

			get pattern() { return this.#pattern; }
		})(pattern) : null;
	}

	terminate(rop) {
		const input = this.#validate(rop?.pattern || rop);
		delete(input[2][input[1]]);
	}

	#validate(pattern) {
		if(typeof pattern === "string")
			pattern = (pattern.endsWith("/") ? pattern : pattern + "/").toLowerCase();
		const key = pattern.toString().toLowerCase();
		if(/^((\/\/?)|(https?:\/\/)).*$/i.test(key))
			return [pattern, key.replace(/^https?:/i, ""), pattern instanceof RegExp ? this.#regexEndpoints : this.#endpoints];
		return [];
	}

	#getEndpoints(url) {
		const endpoints = Object.values(this.#regexEndpoints).filter(e => e.pattern.test(url));
		if(url = this.#endpoints[(url.endsWith("/") ? url : url + '/').replace(/^https?:/i, "")])
			endpoints.unshift(url);
		return endpoints;
	}
}
