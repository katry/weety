"use strict";

var _regexEndpoints, _endpoints, _validate, _getEndpoints, _regexRoutes, _routes, _active, _validate3;

function _classPrivateFieldSet(a, b, c) { var d = _classExtractFieldDescriptor(a, b, "set"); return _classApplyDescriptorSet(a, d, c), c; }

function _classApplyDescriptorSet(a, b, c) { if (b.set) b.set.call(a, c);else { if (!b.writable) throw new TypeError("attempted to set read only private field"); b.value = c; } }

function _classPrivateMethodInitSpec(a, b) { _checkPrivateRedeclaration(a, b), b.add(a); }

function _classPrivateFieldInitSpec(a, b, c) { _checkPrivateRedeclaration(a, b), b.set(a, c); }

function _checkPrivateRedeclaration(a, b) { if (b.has(a)) throw new TypeError("Cannot initialize the same private elements twice on an object"); }

function _classPrivateFieldGet(a, b) { var c = _classExtractFieldDescriptor(a, b, "get"); return _classApplyDescriptorGet(a, c); }

function _classExtractFieldDescriptor(a, b, c) { if (!b.has(a)) throw new TypeError("attempted to " + c + " private field on non-instance"); return b.get(a); }

function _classApplyDescriptorGet(a, b) { return b.get ? b.get.call(a) : b.value; }

function _classPrivateMethodGet(a, b, c) { if (!b.has(a)) throw new TypeError("attempted to get private field on non-instance"); return c; }

/*jshint esversion: 8 */

/*jshint esversion: 8 */

/*jshint esversion: 8 */
Object.defineProperty(Object, "deepCopy", {
  value: a => {
    if (a === null || !(a instanceof Array) && a.constructor !== Object) return a;
    const b = Array.isArray(a) ? [] : {};

    for (let c in a) b[c] = Object.deepCopy(a[c]);

    return b;
  },
  enumerable: false,
  configurable: false,
  writable: false
}), Object.defineProperty(HTMLElement.prototype, "state", {
  get() {
    return !this._state && (this._state = {}), this._state;
  }

}), (() => {
  const a = () => {
    const a = b => {
      b.forEach(b => {
        b.nodeName[0] === "#" || ((b instanceof HTMLAnchorElement || b instanceof HTMLButtonElement) && !b.OBSERVED ? b.addEventListener("click", function (a) {
          const b = this.getAttribute("href") || this.dataset.href || "";

          if (b && !/^((\/\/)|(https?:\/\/)).+/i.test(b)) {
            const c = Object.deepCopy(this.state);
            const d = this.title || this.dataset.title;
            !c.title && d && (c.title = d), history.pushState(c, "", b), a.preventDefault();
          }
        }) : b.nodeName[0] !== "#" && b.nodeName !== "svg" && !b.OBSERVED && (b.childNodes.forEach(c => {
          a(b.childNodes);
        }), new MutationObserver(b => b.forEach(b => a(b.addedNodes))).observe(b, {
          childList: true,
          subtree: false
        })), Object.defineProperty(b, "OBSERVED", {
          value: true,
          enumerable: false,
          configurable: false,
          writable: false
        }));
      });
    };

    a([document.body]);
  };

  document.body ? a() : window.addEventListener("DOMContentLoaded", a);
})(), window.endpoint = new (_regexEndpoints = /*#__PURE__*/new WeakMap(), _endpoints = /*#__PURE__*/new WeakMap(), _validate = /*#__PURE__*/new WeakSet(), _getEndpoints = /*#__PURE__*/new WeakSet(), class {
  constructor() {
    _classPrivateMethodInitSpec(this, _getEndpoints), _classPrivateMethodInitSpec(this, _validate), _classPrivateFieldInitSpec(this, _regexEndpoints, {
      writable: true,
      value: {}
    }), _classPrivateFieldInitSpec(this, _endpoints, {
      writable: true,
      value: {}
    });
    const a = fetch;
    const b = XMLHttpRequest.prototype.open;
    window.fetch = (b, c = {}, d = false) => {
      const e = b instanceof Request ? b.url : b;

      const f = _classPrivateMethodGet(this, _getEndpoints, _getEndpoints2).call(this, e);

      return !d && f.length ? a(b, c).then(a => {
        const d = a.clone();
        if (a.status > 399) for (let b in f) f[b].error(e, a.status, a.statusText);else a.text().then(a => {
          try {
            a = JSON.parse(a);
          } catch (a) {}

          for (let d in f) f[d].load(b.method || c.method || "GET", a);
        });
        return d;
      }) : a(b, c);
    }, XMLHttpRequest.prototype.open = (a => function (c, d, e, f, g, h) {
      const i = b.apply(this, [].slice.call(arguments));

      const j = _classPrivateMethodGet(a, _getEndpoints, _getEndpoints2).call(a, d);

      return !h && j.length && this.addEventListener("readystatechange", a => {
        if (this.readyState === XMLHttpRequest.DONE) {
          if (this.status > 399) {
            for (let a in j) j[a].error(d, this.status, this.statusText);

            return;
          }

          let b = a.responseText;

          try {
            b = JSON.parse(b);
          } catch (a) {}

          for (let a in j) j[a].load(c, b);
        }
      }), i;
    })(this);
  }

  get(a) {
    var b, c;

    const d = _classPrivateMethodGet(this, _validate, _validate2).call(this, a);

    return d.length ? d[2][d[1]] = d[2][d[1]] || new (b = /*#__PURE__*/new WeakMap(), c = /*#__PURE__*/new WeakMap(), class {
      constructor(a) {
        _classPrivateFieldInitSpec(this, b, {
          writable: true,
          value: void 0
        }), _classPrivateFieldInitSpec(this, c, {
          writable: true,
          value: {
            load: {},
            error: []
          }
        }), _classPrivateFieldSet(this, b, a);
      }

      addEventListener(a, b, d = "GET") {
        if (typeof b !== "function") throw new TypeError("Invalid callback");
        if (!_classPrivateFieldGet(this, c)[a]) throw new TypeError(`Invalid listener ${a}`);
        a === "load" ? (!_classPrivateFieldGet(this, c)[a][d] && (_classPrivateFieldGet(this, c)[a][d] = []), _classPrivateFieldGet(this, c)[a][d].push(b)) : _classPrivateFieldGet(this, c)[a].push(b);
      }

      removeEventListener(a, b, d = "GET") {
        if (!_classPrivateFieldGet(this, c)[a]) throw new TypeError(`Invalid listener ${a}`);
        a === "load" ? _classPrivateFieldGet(this, c)[a][d] = (_classPrivateFieldGet(this, c)[a][d] || []).forEach(a => a !== b) : _classPrivateFieldGet(this, c)[a] = _classPrivateFieldGet(this, c)[a].filter(a => a !== b);
      }

      load(a, b) {
        (_classPrivateFieldGet(this, c).load[a] || []).forEach(a => a(b));
      }

      error(a, b, d = "") {
        _classPrivateFieldGet(this, c).error.forEach(c => c(a, b, d));
      }

      get pattern() {
        return _classPrivateFieldGet(this, b);
      }

    })(a) : null;
  }

  terminate(a) {
    const b = _classPrivateMethodGet(this, _validate, _validate2).call(this, a?.pattern || a);

    delete b[2][b[1]];
  }

})(), window.route = new (_regexRoutes = /*#__PURE__*/new WeakMap(), _routes = /*#__PURE__*/new WeakMap(), _active = /*#__PURE__*/new WeakMap(), _validate3 = /*#__PURE__*/new WeakSet(), class {
  constructor() {
    _classPrivateMethodInitSpec(this, _validate3), _classPrivateFieldInitSpec(this, _regexRoutes, {
      writable: true,
      value: {}
    }), _classPrivateFieldInitSpec(this, _routes, {
      writable: true,
      value: {}
    }), _classPrivateFieldInitSpec(this, _active, {
      writable: true,
      value: []
    });

    const a = a => (b, c, d, e = false) => {
      const f = a.call(history, b, c, d);
      return d && !e && this.load(b), f;
    };

    history.pushState = a(history.pushState), history.replaceState = a(history.replaceState), addEventListener('popstate', a => {
      this.load(a.state);
    });
  }

  load(a = null) {
    let b = location.pathname.toLowerCase();
    !b.endsWith("/") && (b += "/");
    const c = Object.values(_classPrivateFieldGet(this, _regexRoutes)).filter(a => a.pattern.test(b));
    _classPrivateFieldGet(this, _routes)[b] && c.unshift(_classPrivateFieldGet(this, _routes)[b]), _classPrivateFieldGet(this, _active).forEach(a => a.unload()), _classPrivateFieldSet(this, _active, []), c.forEach(b => {
      b.load(a), _classPrivateFieldGet(this, _active).push(b);
    });
  }

  get(a) {
    var b, c;

    const d = _classPrivateMethodGet(this, _validate3, _validate4).call(this, a);

    return d[2][d[1]] = d[2][d[1]] || new (b = /*#__PURE__*/new WeakMap(), c = /*#__PURE__*/new WeakMap(), class {
      constructor(a) {
        _classPrivateFieldInitSpec(this, b, {
          writable: true,
          value: void 0
        }), _classPrivateFieldInitSpec(this, c, {
          writable: true,
          value: {
            load: [],
            beforeunload: []
          }
        }), _classPrivateFieldSet(this, b, a);
      }

      addEventListener(a, b) {
        if (typeof b !== "function") throw new TypeError("Invalid callback");
        if (!_classPrivateFieldGet(this, c)[a]) throw new TypeError(`Invalid listener ${a}`);

        _classPrivateFieldGet(this, c)[a].push(b);
      }

      removeEventListener(a, b) {
        if (!_classPrivateFieldGet(this, c)[a]) throw new TypeError(`Invalid listener ${a}`);
        _classPrivateFieldGet(this, c)[a] = _classPrivateFieldGet(this, c)[a].filter(a => a !== b);
      }

      load(a = null) {
        _classPrivateFieldGet(this, c).load.forEach(b => b(a, this));
      }

      unload() {
        _classPrivateFieldGet(this, c).beforeunload.forEach(a => a(this));
      }

      get pattern() {
        return _classPrivateFieldGet(this, b);
      }

    })(d[0]);
  }

  terminate(a) {
    const b = _classPrivateMethodGet(this, _validate3, _validate4).call(this, a?.pattern || a);

    delete b[2][b[1]];
  }

})();

function _validate4(a) {
  typeof a === "string" && (a = (a.endsWith("/") ? a : a + "/").toLowerCase());
  const b = a.toString().toLowerCase();
  if (b[0] !== "/") throw new TypeError(`Invalid route ${b}`);
  return [a, b, a instanceof RegExp ? _classPrivateFieldGet(this, _regexRoutes) : _classPrivateFieldGet(this, _routes)];
}

function _validate2(a) {
  typeof a === "string" && (a = (a.endsWith("/") ? a : a + "/").toLowerCase());
  const b = a.toString().toLowerCase();
  return /^((\/\/?)|(https?:\/\/)).*$/i.test(b) ? [a, b.replace(/^https?:/i, ""), a instanceof RegExp ? _classPrivateFieldGet(this, _regexEndpoints) : _classPrivateFieldGet(this, _endpoints)] : [];
}

function _getEndpoints2(a) {
  const b = Object.values(_classPrivateFieldGet(this, _regexEndpoints)).filter(b => b.pattern.test(a));
  return (a = _classPrivateFieldGet(this, _endpoints)[(a.endsWith("/") ? a : a + '/').replace(/^https?:/i, "")]) && b.unshift(a), b;
}
