# weety
Tiny and pure framework library for complex web development. No compilation need and minimal modification of standard JavaScript and DOM layer.

### Summary of standard JavaScript modifications / changes:
- `Object.deepCopy` - Object prototype extended by method that returns deep copy of first parameter given.
- `HTMLElement.state` - All DOM elements extended by state property which is get only and returns editable associative array.
- `HTMLAnchorElement` and `HTMLButtonElement` behavior overrite - all `a` an `button` elements with `href` (or `data-href` in button) that doesn't start with `//` `http://` or `https://` trigger `history.pushState` instead of redirecting page. This functionality is implemented via `MutationObserver`.

- `window.route` - window extended by `route` functionality that provides routing based on `history.pushState` and `history.replaceState` method calls. <b>Detailed description below.</b>
-`history.pushState` and `history.replaceState` - behavior overwritten to trigger route processing. Also added 'popstate' listener which does the same thing.

- `window.endpoint` - window extended by `endpoint` functionality that provides routing over responses returned via `fetch` and `XMLHttpRequest`. <b>Detailed description below.</b>
-`fetch` and `XMLHttpRequest.prototype.open` - behavior overwritten to trigger endpoint route processing upon recieving a response.

## Instalation
### Download from url:
Tiny version:
```
https://gitlab.com/katry/weety/-/raw/master/dist/weety.min.js
```

Compatibility version (IE9+):
```
https://gitlab.com/katry/weety/-/raw/master/dist/weety.babel.min.js
```

### Use npm
```sh
npm install weety
```

## Initialization
Script tag
```html
<script src="<path-to-your-js-lib-folder>/weety.min.js"></script>
```

or when installed by `npm` \| `yarn`

```html
<script src="/node_modules/weety/dist/weety.min.js"></script>
```

## Using

### Elements

#### HTMLElement
All HTML elements are enhanced by property `state`, which stores non overwritable but editable object (`{}`) to element.

#### HTMLAnchorElement adn HTMLButtonElement
Weety overwrites behavior of elements `<a>` and `<button>` that you should use to navigation through web app routes for navigation is used content of `href` or `data-href` attributes in the order listed.
- When overwritten behavior is applied only when location is written as relative or absolute path without domain, so you can use url starting with one of (`//` `http://`, `https://`) starting strings to enforce default behavior of elements (skip routing and force the redirect on `<a>` element or `<button>` element in form).
- When click event is triggered elements `state` of the element is passed to `pushState` event.
- You can use `title` or `data-title` attributes to provide title text for `pushState` event.

### Useful tidbits
- `Object.deepcopy(a)` - returns copy of first argument

### Routes
Routes functionality is provided by object `window.route`and his following methods:
- `get(pattern)` - get route object by the pattern passed in first argument, pattern must be typed as string or regex.
- `terminate(route_or_pattern)` - terminate and remove all routes listeners.
- `load(state=null)` - invokes matching routes for actual url with optional state argument (should be used on website first load, after all routes are set).

Route object (returned by window.route.get method) method and properties:
- `addEventListener(type, trigger)` - adds listener to the route. First argument is type which shoulbe be one of values `load` or `befoureunload` and second is callback to be triggered.
- `removeEventListener(type, trigger)` - removes listener from route. First argument is type  (`load`, `befoureunload`) and second is callback to be removed.
- `load(state=null)` - method for instant call all route triggers of `load` type with optional state argument.
- `unload()` - method for instant call all route triggers of `beforeunload` type.
- `pattern` - property contains pattern of route (RegExp or string).


### Endpoints
Endpoints functionality is provided by object `window.endpoint`and his following methods:
- `get(pattern)` - get endpoint object by the pattern passed in first argument, pattern must be typed as string or regex.
- `terminate(endpoint_or_pattern)` - terminate and remove all endpoints listeners.

Endpoint object (returned by window.endpoint.get method) method and properties:
- `addEventListener(type, trigger)` - adds listener to the endpoint. First argument is type which shoulbe be one of values `load` or `error` and second is callback to be triggered.
- `removeEventListener(type, trigger)` - removes listener from endpoint. First argument is type  (`load`, `befoureunload`) and second is callback to be removed.
- `load(state=null)` - method for instant call all endpoint triggers of `load` type with optional state argument.
- `error(err, evt)` - method for instant call all endpoint triggers of `error` type. Arguments err and evt are given by fetch and XMLXtthRequest error triggers.
- `pattern` - property contains pattern of endpoint (RegExp or string).


## Example
```html
<!DOCTYPE html>
<html>
<head>
	<title>Weety Test HTML</title>
	<meta charset="utf-8" />

	<script src="build/weety.min.js"></script>
</head>
<body>
	<div class="btc" style="display: none">NO BTC DATA LOADED</div>
	<div class="eth" style="display: none">NO LTC DATA LOADED</div>

	<div>
		<a title="Bitcoin" href="/">bitcoin</a>
		<button title="Litecoin" data-href="/litecoin">litecoin</a>
	</div>
	<button id="load-data">Load</button>

	<script>
	// element state element using example
	document.body.state.test = "stest-state value";

	//routing example
	let btc_route = window.route.get("/");
	btc_route.addEventListener("load", function() {
		let btc = document.querySelector(".btc");
		let eth = document.querySelector(".eth");
		eth.style.display = "none";
		btc.style.display = "block";
	});

	let eth_route = window.route.create(/lite.*/);
	eth_route.addEventListener("load", function() {
		let btc = document.querySelector(".btc");
		let eth = document.querySelector(".eth");
		eth.style.display = "block";
		btc.style.display = "none";
	});


	//endpoints processing example

	const address = "https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=100&page=1&sparkline=false";

	//getting endpoint route
	const endpoint = window.endpoint.get(e);

	//setting load listener
	endpoint.addEventListener("load", function(e) {
		console.log(e);
		let btc = document.querySelector(".btc");
		btc.innerHTML = "1 BTC = $" + e[0].current_price + " USD";
	}, "GET");

	//setting another load listener
	window.endpoint[e].addEventListener("load", function(e) {
		let eth = document.querySelector(".eth");
		eth.innerHTML = "1 ETH = $" + e[2].current_price + " USD";
	});

	//click load button to fetching endpoint url and trigger listeners:
	document.querySelector("button#load-data").addEventListener("click", e => {
		fetch(e);
	});
	</script>
</body>
</html>
```

## TODO List:
- routing for WebSocket and RTCDataChannel messages
- automated tests - DONE
- more examples simple examples
- complex app example
