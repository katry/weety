"use strict";

module.exports = function( grunt ) {
	grunt.loadNpmTasks("grunt-contrib-concat");
	grunt.loadNpmTasks("grunt-babel");
	grunt.loadNpmTasks("grunt-terser");

	grunt.initConfig({
		concat: {
			dist: {
				src: ["src/elements.js", "src/endpoint.js", "src/route.js"],
				dest: "dist/weety.js"
			},
		},

		babel: {
			dist: {
				files: {
					"dist/weety.babel.js": "dist/weety.js"
				}
			}
		},

		terser: {
			options: {
				mangle: {
					properties: {
						regex: /^_/
					}
				}
			},
			dist: {
				files: [
					{
						src: "./dist/weety.js",
						dest: "./dist/weety.min.js",
					},
					{
						src: "./dist/weety.babel.js",
						dest: "./dist/weety.babel.min.js",
					}
				]
			}
		},
	});

	grunt.registerTask("default", ["concat", "babel", "terser"]);
};
