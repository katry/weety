/*jshint esversion: 8 */
require("../src/route.js");
require("../src/route.js");

test('get route', () => {
	const route = window.route.get("/");
	expect(!!route).toEqual(true);
	expect(route).toEqual(window.route.get("/"));
	const route2 = window.route.get("/r2");
	expect(!!route2).toEqual(true);
	expect(route===route2).toEqual(false);
	let fall = false;
	try {
		const bad_route = window.route.get("adfs");
	}
	catch(e) {fall = true;}
	expect(fall).toEqual(true);
});

test('get route regex', () => {
	const classic_route = window.route.get("/test/");
	const route = window.route.get(/test/);

	expect(!!route).toEqual(true);
	expect(!!classic_route).toEqual(true);
	expect(route===classic_route).toEqual(false);
	expect(route===window.route.get(/test/)).toEqual(true);
	expect(route===window.route.get(/test2/)).toEqual(false);
});

test('event trigger', () => {
	const route = window.route.get("/");
	let called = 0;
	const callback = () => {called++;};
	route.addEventListener("load", callback);
	route.addEventListener("beforeunload", callback);
	route.load();
	route.unload();
	expect(called).toEqual(2);
	route.load();
	route.removeEventListener("load", callback);
	route.load();
	route.unload();
	route.removeEventListener("beforeunload", callback);
	route.load();
	route.unload();
	expect(called).toEqual(4);
});

test('terminate route', () => {
	let route = window.route.get("/");
	const callback = () => {called++;};
	let called = 0;
	route.addEventListener("load", callback);
	route.load();
	expect(called).toEqual(1);
	window.route.terminate("/");
	route = window.route.get("/");
	route.load();
	expect(called).toEqual(1);
	route.addEventListener("load", callback);
	route.load();
	expect(called).toEqual(2);
	window.route.terminate(route);
	route = window.route.get("/");
	route.load();
	expect(called).toEqual(2);
});

test('terminate route regex', () => {
	let route = window.route.get(/test/);
	const callback = () => {called++;};
	let called = 0;
	route.addEventListener("load", callback);
	route.load();
	expect(called).toEqual(1);
	window.route.terminate(/test/);
	route = window.route.get(/test/);
	route.load();
	expect(called).toEqual(1);
	route.addEventListener("load", callback);
	route.load();
	expect(called).toEqual(2);
	window.route.terminate(route);
	route = window.route.get(/test/);
	route.load();
	expect(called).toEqual(2);
});

test('route trigger', async () => {
	const route = window.route.get("/test");
	const route2 = window.route.get("/test2");
	let called = 0;
	const callback = () => {called++;};
	route.addEventListener("load", callback);
	route2.addEventListener("load", callback);
	route.addEventListener("beforeunload", callback);
	route2.addEventListener("beforeunload", callback);

	history.pushState(null, null, "/test");
	expect(called).toEqual(1);

	history.pushState(null, null, "/test2/");
	expect(called).toEqual(3); // unload + load
	window.route.terminate(route);
	history.pushState(null, null, "/test/");
	expect(called).toEqual(4);
	history.pushState(null, null, "/");
	expect(called).toEqual(4);
	window.route.terminate("/test2/");
	history.pushState(null, null, "/test2/");
	history.pushState(null, null, "/test");
	expect(called).toEqual(4);
});


test('route regex trigger', async () => {
	const route_classic = window.route.get("/test");
	const route = window.route.get(/\/test.*/);
	const route2 = window.route.get(/\/test2.*/);
	let called = 0;
	const callback = () => {called++;};
	route_classic.addEventListener("load", callback);
	route.addEventListener("load", callback);
	route2.addEventListener("load", callback);
	route.addEventListener("beforeunload", callback);
	route2.addEventListener("beforeunload", callback);

	history.pushState(null, null, "/test");
	expect(called).toEqual(2);

	history.pushState(null, null, "/test2/");
	expect(called).toEqual(5); // unload + load
	window.route.terminate(route);
	history.pushState(null, null, "/test/");
	expect(called).toEqual(8);
	history.pushState(null, null, "/");
	expect(called).toEqual(8);
	window.route.terminate(/\/test2.*/);
	history.pushState(null, null, "/test2/");
	history.pushState(null, null, "/test");
	expect(called).toEqual(9);
});
