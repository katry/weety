/*jshint esversion: 8 */
require("../src/elements.js");
const simulant = require('jsdom-simulant');

test('element state', () => {
	const div = document.createElement("div");
	expect(div instanceof HTMLElement).toEqual(true);
	expect(JSON.stringify(div.state)).toEqual("{}");
	expect(JSON.stringify(document.body.state)).toEqual("{}");
	div.state.jezevec = 543;
	expect(JSON.stringify(div.state)).toEqual("{\"jezevec\":543}");
	try {
		div.state = {"bobr": 345};
	}
	catch(e) {}
	expect(JSON.stringify(div.state)).toEqual("{\"jezevec\":543}");
});

test("object deepcopy", () => {
	const a = {
		bjlk: [
			3,2,4,{jez: 5, div: document, ko: {r: "fdas", p: history, f: () => { return 56; }}}
		],
		w: window,
		e: () => { return 55; },
	};
	const b = {
		bjlk: [
			3,2,4,{jez: 5, div: document, ko: {r: "fdas", p: history, f: () => { return 56; }}}
		],
		w: window,
		e: () => { return 55; },
	};
	const c = a;
	const d = Object.deepCopy(a);


	expect(a === b).toEqual(false);
	expect(a === c).toEqual(true);
	expect(a === d).toEqual(false);

	expect(a.bjlk === b.bjlk).toEqual(false);
	expect(a.bjlk === c.bjlk).toEqual(true);
	expect(a.bjlk === d.bjlk).toEqual(false);

	expect(a.bjlk[0] === b.bjlk[0]).toEqual(true);
	expect(a.bjlk[0] === c.bjlk[0]).toEqual(true);
	expect(a.bjlk[0] === d.bjlk[0]).toEqual(true);

	expect(a.bjlk[3] === b.bjlk[3]).toEqual(false);
	expect(a.bjlk[3] === c.bjlk[3]).toEqual(true);
	expect(a.bjlk[3] === d.bjlk[3]).toEqual(false);

	expect(a.bjlk[3].div === b.bjlk[3].div).toEqual(true);
	expect(a.bjlk[3].div === c.bjlk[3].div).toEqual(true);
	expect(a.bjlk[3].div === d.bjlk[3].div).toEqual(true);

	expect(a.bjlk[3].ko === b.bjlk[3].ko).toEqual(false);
	expect(a.bjlk[3].ko === c.bjlk[3].ko).toEqual(true);
	expect(a.bjlk[3].ko === d.bjlk[3].ko).toEqual(false);

	expect(a.bjlk[3].ko.r === b.bjlk[3].ko.r).toEqual(true);
	expect(a.bjlk[3].ko.r === c.bjlk[3].ko.r).toEqual(true);
	expect(a.bjlk[3].ko.r === d.bjlk[3].ko.r).toEqual(true);

	expect(a.bjlk[3].ko.f()).toEqual(56);
	expect(b.bjlk[3].ko.f()).toEqual(56);
	expect(c.bjlk[3].ko.f()).toEqual(56);
	expect(d.bjlk[3].ko.f()).toEqual(56);

	expect(a.e()).toEqual(55);
	expect(b.e()).toEqual(55);
	expect(c.e()).toEqual(55);
	expect(d.e()).toEqual(55);

});


test("check observer", async() => {
	const a = document.createElement("a");
	const button = document.createElement("button");
	a.href = "";

	const a_href = document.createElement("a");
	a_href.href = "/jezevec-jak-pes";

	const b_href = document.createElement("a");
	b_href.href = "jezevec";

	const c_href = document.createElement("a");
	c_href.href = "./jezevec/jak-pes";

	const d_href = document.createElement("a");
	d_href.href = "jezevec/jak-pes";

	const e_href = document.createElement("a");
	e_href.href = "./jezeEec/jak/pes";

	const f_href = document.createElement("a");
	f_href.setAttribute("data-href", "./jezevec");

	const button_href = document.createElement("button");
	button_href.dataset.href = "/pes-jak-jezevec";

	const a_bh = document.createElement("a");
	const a_ch = document.createElement("a");
	const a_dh = document.createElement("a");
	a_bh.href = "//test.com/ds";
	a_ch.href = "http://test.com/ds";
	a_dh.href = "https://test.com/ds";

	const states = [];
	const callback = jest.fn();

	document.body.appendChild(a);
	document.body.appendChild(a_href);
	document.body.appendChild(b_href);
	document.body.appendChild(c_href);
	document.body.appendChild(d_href);
	document.body.appendChild(e_href);
	document.body.appendChild(f_href);
	document.body.appendChild(button);
	document.body.appendChild(button_href);
	document.body.appendChild(a_bh);
	document.body.appendChild(a_ch);
	document.body.appendChild(a_dh);

	window.history.pushState = ()=> {
		states.push(true);
	};

	await ((ms) => {
		return new Promise(resolve => setTimeout(resolve, ms));
	})(1);

	simulant.fire(a, 'click');
	simulant.fire(button, 'click');
	simulant.fire(a_href, 'click');
	simulant.fire(button_href, 'click');

	simulant.fire(a_bh, 'click');
	simulant.fire(a_ch, 'click');
	simulant.fire(a_dh, 'click');

	simulant.fire(b_href, 'click');
	simulant.fire(c_href, 'click');
	simulant.fire(d_href, 'click');
	simulant.fire(e_href, 'click');
	simulant.fire(f_href, 'click');

	expect(states.length).toEqual(7);
	expect(states[0]).toEqual(true);
	expect(states[1]).toEqual(true);
});
